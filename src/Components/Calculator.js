// import './App.css';
import {Card,Button} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import React, { Component } from 'react'
import AddItem from './AddItem';

export default class Calculator extends Component {
  constructor(props){
    super(props);
    this.state={
      numbers:[],
      val:'a',
      currentNum1:{
        number1:'',
        key:''
      },
      currentNum2:{
        number2:'',
        key:''
      }
     
    };
    this.handleInput1=this.handleInput1.bind(this);
    this.handleInput2=this.handleInput2.bind(this);
    this.addItems=this.addItems.bind(this);

  }

  handleInput1=(e)=>{
    this.setState({
      currentNum1:{
        number1: e.target.value,
        key: Date.now
      }
    });
  }
  handleInput2=(e)=>{
    this.setState({
      currentNum2:{
        number2: e.target.value,
        key: Date.now
      }
    });
  }
  selecteds=(e)=>{
    this.setState({
      val: e.target.value
    })
  }
  addItems = event => {
    event.preventDefault();
    var num1 = parseInt(this.state.currentNum1.number1);
    var num2 = parseInt(this.state.currentNum2.number2);
    
    var selected = this.state.val;
    var num=0;
    console.log(selected);
    if(isNaN(num1)||isNaN(num2)){
      alert("Cannot Calculate Number");
      return false;
    }
    if(selected==="a"){
      num=num1+num2
    }else if(selected==="b"){
      num=num1-num2
    }else if(selected==="c"){
      num=num1*num2
    }else if(selected==="d"){
      num=num1/num2
    }else if(selected==="e"){
      num=num1%num2
    }
    
    if(num!==""){
      const newNum=[...this.state.numbers,num];
      this.setState({
        numbers:newNum,
        // currentNum1:{
        //   number1:'',
        //   key:''
        // },
        // currentNum2:{
        //   number2:'',
        //   key:''
        // }
      })
    }
   };

  render() {
      
    return (
      <div className="container"><br/>
        <div className="row">
          <div className="col-md-4">  
          <Card>
          <Card.Img variant="top" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSaO94rg8GPAGC0hE3dyAert-IavQkQ9NzK8eYnTx1J5HFKVyxM&usqp=CAU" />
            <Card.Body>
            <form onSubmit={this.addItems}>
                  <div className="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input
                      type="text"
                      className="form-control"
                      name="Number1"
                      value={this.state.currentNum1.number1}
                      onChange={this.handleInput1}/>
                  </div> <br/>
                  <div className="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input
                      type="text"
                      className="form-control"
                      name="Number2"
                      value={this.state.currentNum2.number2}
                      onChange={this.handleInput2}/>
                  </div><br/>
                  <div className="input-group mb-2 mr-sm-2 mb-sm-0">
                    <select className="form-control" onChange={this.selecteds} defaultValue={this.state.value}>
                      <option value="a">+ Plus</option>
                      <option value="b">- Substract</option>
                      <option value="c">* Multiplication</option>
                      <option value="d">/ Division</option>
                      <option value="e">% Module</option>
                    </select>
                  </div> <br/>
                  <Button variant="primary" onClick={this.addItems}>Calculator</Button>
              </form>
              </Card.Body>
            </Card>
          </div>
             
            <div className="col-md-4">
              <h4>Result History</h4>
              <AddItem numbers={this.state.numbers}></AddItem>
            </div> 
        </div>
      </div>
      
    )
  }
}
